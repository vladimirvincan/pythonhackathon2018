"""This module is main module for contestant's solution."""

from hackathon.utils.control import Control
from hackathon.utils.utils import ResultsMessage, DataMessage, PVMode, \
    TYPHOON_DIR, config_outs
from hackathon.framework.http_server import prepare_dot_dir
from hackathon.energy.energy_math import current_load as current_load_function
from hackathon.energy.energy_math import solar_produciton as solar_production_function

def min_to_time(minute):
    return (float(minute % 1440)) / 60

def predict_load_divergencies(msg):
    time = (float(msg.id % 1440)) / 60
    if current_load_function(time) > 2:
       return (msg.current_load-2) / (current_load_function(time)-2)
    return -1

def predict_solar_divergencies(msg):
    time = (float(msg.id % 1440)) / 60
    if solar_production_function(time) > 0:
       return (msg.solar_production) / (solar_production_function(time))
    return -2

def get_hourly_battery_usage(power):
    if power <= 5.0:
        return power
    elif power*0.6 <= 5.0:
        return power*0.6
    elif power*0.2 <= 5.0:
        return power*0.2
    return 0.0

def worker(msg: DataMessage) -> ResultsMessage:
    """TODO: This function should be implemented by contestants."""
    # Details about DataMessage and ResultsMessage objects can be found in /utils/utils.py

#====================================================== CONSTANTS
    battery_minute_capacity = 5.0 / (20 * 60)  # koliko treba da ima energije da izgura minut na 5kW
    coefL1 = 0.2
    coefL2 = 0.4
    coefL3 = 0.4

    battery_max_power = 5.0


    L1OTPen = 20
    L2OTPen = 4
    L3OTPen = 0
    L1Pen_per_minute = 1
    L2Pen_per_minute = 0.4
    L3Pen_per_minute = 0.3

    offset = 1

    ponoc_minuti = 0
    sedamh_minuti = 420 - offset
    jedanaesth_minuti = 660 - offset
    sedamnaesth_minuti = 1020 - offset
    devethaesth_minuti = 1140 - offset
    dvadesettrih_minuti = 1380 - offset

    minutiMOD = 1440

    #============================================================
    minuti = msg.id
    if not hasattr(worker, "day"):
        worker.day = 0  # it doesn't exist yet, so initialize it
    if not hasattr(worker, "blackout_danas"):
        worker.blackout_danas = False
    if not hasattr(worker, "previous_grid_status"):
        worker.previous_grid_status = 1
    if not hasattr(worker, "calculated_load_divergence"):
        worker.calculated_load_divergence = -1.0
    if not hasattr(worker, "calculated_solar_divergence"):
        worker.calculated_solar_divergence = -1.0
    if not hasattr(worker, "battery_treshold"):
        worker.battery_treshold = 0.25

    power_from_battery = min(5.0,
                             5.0 * msg.bessSOC / battery_minute_capacity)  # uzmi energije iz baterije koliko moze da izgura u ovom minutu
    # note: ako je prazna, bice svakako 0
    power_missing_from_battery = min(5.0, 5.0 * (
                1.0 - msg.bessSOC) / battery_minute_capacity)  # proveri da ne preteras sa punjenjem u ovom minutu
    power_from_battery_above_treshold = min(5.0, 5.0 * (msg.bessSOC - worker.battery_treshold) / battery_minute_capacity)

    if minuti % 1440 == 1: ## dan ima 1440 minuta i 1441. ce se apdejtovati
        worker.day = worker.day + 1
        worker.blackout_danas = False
    minuti_danas = minuti % minutiMOD
    load_one = True
    load_two = True
    load_three = True
    power_reference = 0.0
    pv_mode = PVMode.ON

    #============================================================    


    # sve (osim prvih 10) msg.current_load i msg.solar_production zameniti kasnije sa Bicijevim predvidjenim

    if minuti_danas == ponoc_minuti+180:
        worker.calculated_load_divergence = predict_load_divergencies(msg)
    elif minuti_danas > ponoc_minuti + 181 and minuti_danas <= ponoc_minuti + 186:
        worker.calculated_load_divergence = (worker.calculated_load_divergence * (minuti_danas - ponoc_minuti-180) + predict_load_divergencies(msg))/ (minuti_danas + 1 - ponoc_minuti - 180)
    elif minuti_danas == ponoc_minuti + 187:
        worker.calculater_load_divergence = worker.calculated_load_divergence * 7.0 / 3.0
        

    if minuti_danas == sedamh_minuti + 1:
        worker.calculated_solar_divergence = predict_solar_divergencies(msg)
    elif minuti_danas > sedamh_minuti + 1 and minuti_danas <= sedamh_minuti + 6:
        worker.calculated_solar_divergence = (worker.calculated_solar_divergence * (minuti_danas - sedamh_minuti) + predict_solar_divergencies(msg))/ (minuti_danas + 1 - sedamh_minuti)
    elif minuti_danas == sedamh_minuti + 7:
        worker.calculated_solar_divergence = worker.calculated_solar_divergence * 7.0
        most_power_consumed_during_blackout = 0.0
        for i in range(sedamh_minuti + 7, sedamh_minuti + 67):
            most_power_consumed_during_blackout += get_hourly_battery_usage((worker.calculated_load_divergence*current_load_function(min_to_time(i)) - worker.calculated_solar_divergence*solar_production_function(min_to_time(i)))/60)
        tmp_power_consumed = most_power_consumed_during_blackout
        for i in range(sedamh_minuti+67, minutiMOD):
            tmp_power_consumed = tmp_power_consumed - \
            get_hourly_battery_usage((worker.calculated_load_divergence*current_load_function(min_to_time(i-60)) - worker.calculated_solar_divergence*solar_production_function(min_to_time(i-60)))/60)+ \
            get_hourly_battery_usage((worker.calculated_load_divergence*current_load_function(min_to_time(i)) - worker.calculated_solar_divergence*solar_production_function(min_to_time(i)))/60)
            if tmp_power_consumed > most_power_consumed_during_blackout:
                most_power_consumed_during_blackout = tmp_power_consumed
        worker.battery_treshold = min(most_power_consumed_during_blackout / 20, 0.25)

    if msg.grid_status:

        L1 = True
        L2 = True
        L3 = True
        pv_mode = PVMode.ON

        if minuti_danas >= ponoc_minuti and minuti_danas <= sedamh_minuti:
            power_reference = - power_missing_from_battery

        elif minuti_danas <= jedanaesth_minuti:
            if msg.solar_production >= msg.current_load:
                power_reference = -min(msg.solar_production - msg.current_load, power_missing_from_battery)
            elif worker.blackout_danas == False:  # ako sam vec imao blackout, bateriju trosim maximalno,
                # a napunicu je od 23h - 24h, kad je jeftino, a sada da ustedim na skupoj stuji
                power_reference = power_from_battery_above_treshold
            elif msg.solar_production + power_from_battery >= msg.current_load:
                power_reference = msg.current_load - msg.solar_production
            else:
                power_reference = power_from_battery


        elif minuti_danas <= sedamnaesth_minuti:  # kada se predvide funkcije, resiti problem u napomeni
            if msg.solar_production >= msg.current_load:
                power_reference = -min(msg.solar_production - msg.current_load, power_missing_from_battery)
            elif worker.blackout_danas == False:  # ako sam vec imao blackout, bateriju trosim maximalno,
                # a napunicu je od 23h - 24h, kad je jeftino, a sada da ustedim na skupoj stuji
                power_reference = power_from_battery_above_treshold
            elif msg.solar_production + power_from_battery >= msg.current_load:
                power_reference = msg.current_load - msg.solar_production
            else:
                power_reference = power_from_battery

        elif minuti_danas <= devethaesth_minuti:  # trebalo bi da je uvek isto kao 7h - 11h
            if msg.solar_production >= msg.current_load:
                power_reference = -min(msg.solar_production - msg.current_load, power_missing_from_battery)
            elif worker.blackout_danas == False:  # ako sam vec imao blackout, bateriju trosim maximalno,
                # a napunicu je od 23h - 24h, kad je jeftino, a sada da ustedim na skupoj stuji
                power_reference = power_from_battery_above_treshold
            elif msg.solar_production + power_from_battery >= msg.current_load:
                power_reference = msg.current_load - msg.solar_production
            else:
                power_reference = power_from_battery

        elif minuti_danas <= dvadesettrih_minuti:
            if worker.blackout_danas == False:
                power_reference = power_from_battery_above_treshold
            else:
                power_reference = power_from_battery

        else:
            if worker.day != 5:
                power_reference = -power_missing_from_battery
            else:
                power_reference = power_from_battery

    else: #ako je black out
        if worker.previous_grid_status == 1:
            worker.blackout_danas = True

        if msg.solar_production < msg.current_load and pv_mode == PVMode.ON: #ne moze solar sam da izgura
            load_with_solar = msg.current_load - msg.solar_production
            if load_with_solar <=power_from_battery: #ako baterija moze da izgura i nije prazna
                power_reference=load_with_solar

            elif load_with_solar > power_from_battery: #5kW, ako baterija ne moze da izgura
                load_one = False
                load_two = False
                load_three = False

                current_power_reference = 0
                if (coefL1 * msg.current_load <= power_from_battery + msg.solar_production):
                    load_one = True
                    current_power_reference = coefL1 * msg.current_load
                    if (coefL2 * msg.current_load <= power_from_battery - current_power_reference + msg.solar_production):
                        load_two = True
                        current_power_reference += coefL2 * msg.current_load
                        if (coefL3 * msg.current_load <= power_from_battery - current_power_reference + msg.solar_production):
                            load_three = True
                            current_power_reference += coefL3 * msg.current_load

                power_reference = current_power_reference-msg.solar_production

            else:
                load_one = True
                load_two = True
                load_three = True
                power_reference = load_with_solar
        else: # ako ima dovoljno solar productiona
            load_one = True
            load_two = True
            load_three = True
            if msg.solar_production - msg.current_load <= power_missing_from_battery:
                power_reference = - power_missing_from_battery #manja vrj od nule, baterija se puni
            else: # ako ima previse solar productiona
                pv_mode = PVMode.OFF
                if (coefL1 * msg.current_load <= power_from_battery):
                    load_one = True
                    current_power_reference = coefL1 * msg.current_load
                    if (coefL2 * msg.current_load <= power_from_battery - current_power_reference):
                        load_two = True
                        current_power_reference += coefL2 * msg.current_load
                        if (coefL3 * msg.current_load <= power_from_battery - current_power_reference):
                            load_three = True
                            current_power_reference += coefL3 * msg.current_load
                power_reference = current_power_reference

    worker.previous_grid_status = msg.grid_status #za iduci minut, radi trackovanja blackouta

    return ResultsMessage(data_msg=msg,
                          load_one=load_one,
                          load_two=load_two,
                          load_three=load_three,
                          power_reference=power_reference,
                          pv_mode=pv_mode)


def run(args) -> None:
    prepare_dot_dir()
    config_outs(args, 'solution')

    cntrl = Control()

    for data in cntrl.get_data():
        cntrl.push_results(worker(data))





