import pandas as pd
import json
import matplotlib.pyplot as plt
# {"gridStatus": 1, "buyingPrice": 3, "sellingPrice": 3, "currentLoad": 4.332437198131057, "solarProduction": 0},
file = r"C:\Users\bici\Desktop\pythonhackathon2018\data\profiles.json"
with open(file) as json_file:
    dict_json = json.load(json_file)

plt.style.use('seaborn-dark-palette')
fig = plt.figure()

plt.axvline(x=420,  ymin=0, ymax = 8, linewidth=1, color='k')
plt.axvline(x=660,  ymin=0, ymax = 8, linewidth=1, color='k')
plt.axvline(x=1020, ymin=0, ymax = 8, linewidth=1, color='k')
plt.axvline(x=1140, ymin=0, ymax = 8, linewidth=1, color='k')
plt.axvline(x=1380, ymin=0, ymax = 8, linewidth=1, color='k')
ax1 = fig.add_subplot(111)
ax2 = fig.add_subplot(111)
ax3 = fig.add_subplot(111)
ax4 = fig.add_subplot(111)
ax5 = fig.add_subplot(111)
# ax1.title.set_text('gridStatus')
# ax1.title.set_text('buyingPrice')
# ax1.title.set_text('sellingPrice')
# ax1.title.set_text('currentLoad')
# ax2.title.set_text('solarProduction')
#ax1.plot(range(7200), [elem['gridStatus']for elem in dict_json], color = 'r', label='gridStatus')
#ax2.plot(range(7200), [elem['buyingPrice']for elem in dict_json], color = 'g', label='buyingPrice')
#ax3.plot(range(7200), [elem['sellingPrice']for elem in dict_json], color = 'b', label='sellingPrice')
ax4.plot(range(7200), [elem['currentLoad']for elem in dict_json], color = 'c', label='currentLoad')
ax5.plot(range(7200), [elem['solarProduction']for elem in dict_json], color = 'm', label='solarProduction')




fig.legend(loc='upper left')

plt.show()


 
